﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Facturat.Models
{
    public class Tipo
    {
       [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Nombre { get; set; }
    }
}
