using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Facturat.Models
{
    [BsonIgnoreExtraElements()]
    public class Factur
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id {get; set;}
        public string Name {get; set;}
		public string Tipo { get; set; }
        public double Amount {get; set;}
    }
}