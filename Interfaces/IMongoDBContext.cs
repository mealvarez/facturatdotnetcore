using System.Collections.Generic;
using System.Threading.Tasks;
using Facturat.Models;
using MongoDB.Driver;

namespace Facturat.Interfaces
{
    public interface IMongoDBContext
    {
        Task<IEnumerable<Factur>> GetAllFacturs();
        Task AddFactur(Factur item);
        Task<DeleteResult> RemoveFactur(string id);

        IMongoCollection<Factur> Facturs { get; }
    }
}