﻿using Microsoft.AspNetCore.Mvc;

namespace Facturat.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Sobre mi.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Contactame.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
