using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Facturat.Models;
using Facturat.Interfaces;

namespace Facturat.Controllers
{
    public class FacturatController : Controller 
    {
        private readonly IMongoDBContext _dbContext;
        public FacturatController(IMongoDBContext facturRepository)
        {
            _dbContext = facturRepository;
        }
        public IActionResult index()
        {
            var all = GetFacturInternal();
            return View(all);
        }
        private async Task<IEnumerable<Factur>> GetFacturInternal()
        {
            return await _dbContext.GetAllFacturs();
        }
        [HttpPost]
        public IActionResult AddFactur([FromBody] Factur factur)
        {
            
            try
            {
                AddOneFactur(factur);
            }
            catch (System.Exception)
            {
                //log
                return null;
            }
            
            return Json(factur);
        }

        // DELETE api/notes/23243423
        [HttpDelete]
        public IActionResult Delete(string id)
        {
             try
            {
                RemoveOneFactur(id);
            }
            catch (System.Exception)
            {
                //log
                return null;
            }
            
            return Json(id);
        }

        [HttpPost]
        public IActionResult Add(Factur entity)
        {
            entity.Id = Guid.NewGuid().ToString();

            _dbContext.Facturs.InsertOne(entity);

            return Redirect("/");
        }
        
        private async void AddOneFactur(Factur item)
        {
            try
            {
                await _dbContext.AddFactur(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        private async void RemoveOneFactur(string id)
        {
            try
            {
                await _dbContext.RemoveFactur(id);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
    
#region test
        [HttpGet]
        public Task<IEnumerable<Factur>> Get()
        {
            return GetFacturInternal();
        }
#endregion        
    }
}