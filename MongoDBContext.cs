using MongoDB.Driver;
using System;
using Facturat.Models;
using Facturat.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Facturat
{
    public class MongoDBContext : IMongoDBContext
    { 
        public static string ConnectionString { get; set; } 
        public static string DatabaseName { get; set; } 
        public static bool IsSSL { get; set; } 

        private IMongoDatabase _database { get; } 

        public MongoDBContext() 
        { 
            try 
            { 
                MongoClientSettings settings = MongoClientSettings.FromUrl(new MongoUrl(ConnectionString)); 
                if (IsSSL) 
                { 
                    settings.SslSettings = new SslSettings { EnabledSslProtocols = System.Security.Authentication.SslProtocols.Tls12 }; 
                } 
                var mongoClient = new MongoClient(settings); 
                _database = mongoClient.GetDatabase(DatabaseName); 
            } 
            catch (Exception ex) 
            { 
                throw new Exception("Can not access to db server.", ex); 
            } 
        } 

        public IMongoCollection<Factur> Facturs 
        { 
            get 
            { 
                return _database.GetCollection<Factur>("Facturs"); 
            } 
        } 

         public async Task<IEnumerable<Factur>> GetAllFacturs()
         {
            try
            {
                return await Facturs.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
         }

        public async Task AddFactur(Factur item)
        {
            try
            {
                await Facturs.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<DeleteResult> RemoveFactur(string id)
        {
            try
            {
                return await Facturs.DeleteOneAsync(
                     Builders<Factur>.Filter.Eq("_id", MongoDB.Bson.ObjectId.Parse(id)));
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
    } 
}
