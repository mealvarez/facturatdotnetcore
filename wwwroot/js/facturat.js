$(function(){
    $('totalamount').val('0');
    //recuperar datos almacenados y mostrar lista factur
    
    
    $('#add').click(function(e) {
        if( !isEmptyOrSpaces($('#amount').val()) && 
            !isEmptyOrSpaces($('#amount').val()) ) {
            var deuda = {
                'Amount' : parseFloat($('#amount').val()).toFixed(2),
                'Name' : $('#name').val(),
                'Tipo' : $('#tipo').val()
            };
            
            //ajax para guardar en el servidor
            save(deuda);
        } else {
            alert("completa todo gato");
        }
    });
    
    $("input").keyup(function(event){
        if(event.keyCode == 13){
            $("#add").click();
        }
    });
});

function save(deuda){
	$.ajax({
	    type        : "POST",
	    url         : "/Facturat/AddFactur",
        dataType    : "json",
        contentType : "application/json; charset=utf-8;",
        data        : JSON.stringify(deuda),
        success     : function(result) {
            
            if(result) {
                $('.alert').text("Se ha guardado el nuevo servicio").fadeIn(400).delay(3000).fadeOut(400);
                
                $('#facturs').append('<li id="'+ result.id +'" class="list-group-item element"></li>');
                
                $('#' + result.id).append('<span>' + result.name + '</span> - <span>' + result.amount + '</span>');
                $('#' + result.id).append('<button class="btnDelete btn btn-danger" onclick="deleteFactur(this)"> <i class="fa fa-trash-o" aria-hidden=true></i></button>');
                
                calculateTotal(result.id);
                
                $('#amount').val("");
                $('#name').val("");
            }
        }
    });  
};

function deleteFactur(e){
    var id = e.parentElement.id;
    $.ajax({
        type        : "DELETE",
	    url         : "/Facturat/Delete/" + id,
        success     : function(result) {
            //result = id borrado
            if(result){
                $('.alert').text("Se ha borrado el servicio").fadeIn(400).delay(3000).fadeOut(400);
                calculateTotal(result, true);                                
                $('#' + result).remove();   
            }
        }
    })
}

function calculateTotal(id, resta = false) {
    if($('#' + id)){
        
        var total = parseFloat($('#totalamount').text().replace(",", '.')).toFixed(2);
        if(!total) {
            total = 0.00;
        } 
        
        //obtener servicio(elemento li) por id
        var currentElement = $('#' + id).children();
        //indice 0 = name
        var name = $(currentElement[0]).text();    
        //indicce 1 = amount
        var amount =  parseFloat($(currentElement[1]).text().replace(",", '.')).toFixed(2) ;
        
        total = resta ? total - amount : total + amount; 
        if(total < 0 ){
            $('#totalamount').text(0);
        }else{
            $('#totalamount').text(total.toFixed(2));
        }
    } else {
        $('#totalamount').text('0');
    }
}


function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}